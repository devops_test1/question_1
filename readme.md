# Start maria db vm
- `$ cd vagrant`
- `$ vagrant up`

# Run playbook
- Create virtualenv and install requirements.txt
- (eg: `$ virtualenv ENV && source ENV/bin/activate && pip install -r requirements.txt`)
- Run playbook:
  - `$ cd ansible`
  - `$ ansible-playbook -i hosts --ask-vault maria_db_playbook.yml`


